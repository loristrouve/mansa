# Packages

import pandas as pd
import warnings
warnings.filterwarnings("ignore")
from src.preprocessing import *
from src.inference import *
from src.config import *
from src.training import *
from src.create_feature import *

def all_preparation_data_training(account, transaction):
    '''
    Function that does the entire process from data preparation to training. All the functions present is in preprocessing.py
    :param account: Dataframe pandas;
    :param transaction: Dataframe pandas;
    :return: 2 tables; X for features and Y for target
    '''

    # Transform var object into date
    transaction["date"] = pd.to_datetime(transaction["date"])
    account["update_date"] = pd.to_datetime(account["update_date"])

    # preprocessing of data
    transaction_pp = preprocessing(transaction)
    indics = calculate_indic(transaction_pp, account)

    # Keep minimum 180 days of history
    account_restreint, transaction_resreint = keep_180days_history(transaction_pp, account, indics)

    # 2 tables of data for modeling
    base_outgoing_30, base_outgoing_daily = database_outgoing(transaction_resreint, account_restreint, 20)
    base_balance = build_balance_history(transaction_resreint, account_restreint, 20)

    # Arranging all input data to minimum 12 months of history (add 0 if empty)
    base_balance_arrange = arrange_n_months(base_balance, 20, "balance")
    base_outgoing_arrange = arrange_n_months(base_outgoing_30, 20, "outgoing")

    # Concat 2 tables : outgoing and balance
    data_final = create_concat_table(base_balance_arrange, base_outgoing_arrange)
    data_final = data_final.reset_index()

    # Add features
    data_final2 = add_features(base_balance_arrange, base_outgoing_daily, data_final)
    data_final2 = data_final2.set_index('index')

    # The last 2 datas : X and Y for training
    X_data_final = data_final2.drop(columns=["Y"])
    Y_data_final = data_final2["Y"]

    return X_data_final, Y_data_final

def all_preparation_data_inference(account, transaction):
    '''
    Function that does the entire process from data preparation to inference. All the functions present is in preprocessing.py
    :param account: Dataframe pandas;
    :param transaction: Dataframe pandas;
    :return: 2 tables; X for features and Y for target
    '''

    # Transform var object into date

    print(account)
    print(transaction)

    transaction["date"] = pd.to_datetime(transaction["date"])
    account["update_date"] = pd.to_datetime(account["update_date"])

    transaction["account_id"] = 0
    account["id"] = 0

    # preprocessing of data
    transaction_pp = preprocessing(transaction)

    # 2 tables of data for modeling
    base_outgoing_30, base_outgoing_daily = database_outgoing(transaction_pp, account, 20)
    base_balance = build_balance_history(transaction_pp, account, 20)

    # Arranging all input data to minimum 20 months of history (add 0 if empty)
    base_balance_arrange = arrange_n_months(base_balance, 20, "balance")
    base_outgoing_arrange = arrange_n_months(base_outgoing_30, 20, "outgoing")

    # Concat 2 tables : outgoing and balance
    data_final = create_concat_table(base_balance_arrange, base_outgoing_arrange, training=False)
    data_final = data_final.reset_index()

    # Add features
    data_final2 = add_features(base_balance_arrange, base_outgoing_daily, data_final)
    data_final2 = data_final2.set_index('index')

    # The last 2 datas : X and Y for training
    X_data_test = data_final2

    return X_data_test


if __name__ == "__main__" :
    '''
    This code is used to train the model
    '''

    indic_mean = 0
    indic_std = 0

    account = pd.read_csv(DATA_ACCOUNT, sep=",", encoding="utf8")
    transaction = pd.read_csv(DATA_TRANSACTIONS, sep=",", encoding="utf8")

    X_data, Y_data = all_preparation_data_training(account, transaction)

    model_xgboost, indic_mae, indic_r2, indic_r2_adj = simple_training(X_data, Y_data)

    print(indic_mae)
    print(indic_r2)
    print(indic_r2_adj)
