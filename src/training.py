from sklearn.model_selection import train_test_split
import xgboost as xg
from sklearn.metrics import mean_absolute_error as MAE
from sklearn.metrics import r2_score
from joblib import dump
from joblib import load


def simple_training(X_data, Y_data):
    '''
    Function for training
    :param X_data: Dataframe pandas; output 1 of all_preparation_data_training in second.py
    :param Y_data: Dataframe pandas; output 2 of all_preparation_data_training in second.py
    :return: 4 ;
    xg_m : The model after training for inference
    indic_mae : To view the indicator of MAE
    indic_r2 : To view the indicator of R2
    indic_r2_adj : To view the indicator of R2 ADJ
    '''

    # Split data for training ; Arbitrarily, we choose 0.2 for test_size of train/test
    X_train, X_test, y_train, y_test = train_test_split(X_data, Y_data, test_size=0.2, random_state=42)

    # Normalize all data with the X_train indicators
    mean = X_train.mean(axis=0)
    X_train -= mean
    std = X_train.std(axis=0)
    X_train /= std

    X_test -= mean
    X_test /= std

    # Instantiation
    data_dmatrix = xg.DMatrix(data=X_train, label=y_train)

    # Parameter chosen after Bayesian optimization in the notebook
    params = {"objective": 'reg:squarederror', "colsample_bytree": 0.2, "learning_rate": 0.08, "max_depth": 4,
              "alpha": 16}
    xg_m = xg.train(params=params, dtrain=data_dmatrix, num_boost_round=70)

    data_dmatrix_test = xg.DMatrix(data=X_test)
    preds = xg_m.predict(data_dmatrix_test)

    # Simple indicators
    indic_mae = MAE(y_test, preds)
    indic_r2 = r2_score(y_test, preds)
    indic_r2_adj = 1 - (1 - indic_r2) * (X_train.shape[0] - 1) / (X_train.shape[0] - X_train.shape[1] - 1)

    # We save indicators (mean + std for apply) and save model for inference
    dump(xg_m, "../models/model.joblib.dat")
    mean.to_csv('../models/mean.csv')
    std.to_csv("../models/std.csv")

    print("Saved model to: models/model.joblib.dat")

    return xg_m, indic_mae, indic_r2, indic_r2_adj