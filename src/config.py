# config.py

DATA_ACCOUNT = "../input/accounts.csv"
DATA_TRANSACTIONS = "../input/transactions.csv"

MODEL_OUTPUT = "models/model.joblib.dat"

MEAN_OUTPUT = "models/mean.csv"
STD_OUTPUT = "models/std.csv"
