import pandas as pd

def add_features(balance, outgoing_daily, base_final):
    '''
    Function that creates features of final table

    :param balance: Dataframe Pandas: A Dataframe from the functions : build_balance_history and n_arrange_month ; n_arrange_month(build_balance_history)
    :param outgoing_daily: Dataframe Pandas: A Dataframe from the functions : database_outgoing and arrange_n_months ; n_arrange_month(database_outgoing)
    :param base_final: Dataframe Pandas: A final base already built and we add the calculated features
    :return: Dataframe Pandas: A new final base with new features
    '''

    # 1. If the balance was negative last 6-12 months (binary)
    # 2. Max outgoing last 6-12 months
    # 3. Mean/std balance and outgoing last 6-12 months

    database = pd.DataFrame()

    for i in outgoing_daily["account_id"].unique():

        balance_neg_6 = 0
        balance_neg_12 = 0

        max_outgoing_6 = 0
        mean_outgoing_6 = 0
        std_outgoing_6 = 0

        max_outgoing_12 = 0
        mean_outgoing_12 = 0
        std_outgoing_12 = 0

        if len(balance[(balance["id"] == i) & (balance["int_30"] <= 6) & (balance["balance"] < 0)]) > 0:
            # Balance negative 6-12 months
            balance_neg_6 = len(balance[(balance["id"] == i) & (balance["int_30"] <= 6) & (balance["balance"] < 0)])
            # Balance negative 12 months
            balance_neg_12 = len(balance[(balance["id"] == i) & (balance["int_30"] <= 12) & (balance["balance"] < 0)])

        if len(outgoing_daily[(outgoing_daily["account_id"] == i) & (outgoing_daily["int_30"] <= 6)]["amount"]) > 0:
            # Max outgoing last 6 month
            max_outgoing_6 = min(
                outgoing_daily[(outgoing_daily["account_id"] == i) & (outgoing_daily["int_30"] <= 6)]["amount"])
            # Mean outgoing last 6 months
            mean_outgoing_6 = outgoing_daily[(outgoing_daily["account_id"] == i) & (outgoing_daily["int_30"] <= 6)][
                "amount"].mean()
            # std outgoing last 6 months
            std_outgoing_6 = outgoing_daily[(outgoing_daily["account_id"] == i) & (outgoing_daily["int_30"] <= 6)][
                "amount"].std()

        if len(outgoing_daily[(outgoing_daily["account_id"] == i) & (outgoing_daily["int_30"] <= 12)]["amount"]) > 0:
            # Max outgoing last 12 months
            max_outgoing_12 = min(
                outgoing_daily[(outgoing_daily["account_id"] == i) & (outgoing_daily["int_30"] <= 12)]["amount"])
            # Mean outgoing last 12 months
            mean_outgoing_12 = outgoing_daily[(outgoing_daily["account_id"] == i) & (outgoing_daily["int_30"] <= 12)][
                "amount"].mean()
            # std outgoing last 12 months
            std_outgoing_12 = outgoing_daily[(outgoing_daily["account_id"] == i) & (outgoing_daily["int_30"] <= 12)][
                "amount"].std()

        database = database.append(pd.DataFrame(data=[
            [i, balance_neg_6, balance_neg_12, max_outgoing_6, max_outgoing_12, mean_outgoing_6, mean_outgoing_12,
             std_outgoing_6, std_outgoing_12]], columns=["id", "balance_neg_6", "balance_neg_12", "max_outgoing_6",
                                                         "max_outgoing_12", "mean_outgoing_6", "mean_outgoing_12",
                                                         "std_outgoing_6", "std_outgoing_12"]))

    base_final = base_final.merge(database, left_on='index', right_on='id', how='left').drop(columns=["id"])

    return base_final