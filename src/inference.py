#inference.py

from joblib import load
from src.config import *
import xgboost as xg
import pandas as pd

def inference_model(data):
    '''

    Function that makes inference

    :param data: Dataframe pandas: The data to predict from the main file (root function) ;
    :return: numpy.ndarray: it's result from the predict
    '''

    # Load model from file
    loaded_model = load(MODEL_OUTPUT)
    print("Loaded model from: models/model.joblib.dat")

    mean = pd.read_csv(MEAN_OUTPUT, header = None, index_col = 0, squeeze = True)
    mean = mean.iloc[1:]
    std = pd.read_csv(STD_OUTPUT, header = None, index_col = 0, squeeze = True)
    std = std.iloc[1:]

    data -= mean
    data /= std

    data_dmatrix = xg.DMatrix(data=data)

    # Make predictions for test data
    predictions = loaded_model.predict(data_dmatrix)

    return predictions