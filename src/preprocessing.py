# Importing Package

import warnings
warnings.filterwarnings("ignore")

from scipy import stats
import numpy as np
import pandas as pd


def preprocessing(data):
    '''
    Function that removes duplicates, values equal to 0 and outliers
    :param data: Dataframe pandas
    :return: Dataframe pandas
    '''
    print("Start with : " + str(data.shape))

    # drop duplicates
    data = data.drop_duplicates()
    print("After drop duplicate : " + str(data.shape))

    # drop amount equal to 0
    data = data.drop(data[data.amount == 0].index)
    print("After amount equal to 0 : " + str(data.shape))

    # drop outliers
    data['z_score'] = stats.zscore(data['amount'])
    data.drop(data[data['z_score'] > 3].index)
    del data['z_score']
    print("After drop outliers : " + str(data.shape))

    return data


def calculate_indic(transaction, account):
    '''
    Function which calculates for each id, the number of transactions and the transaction history
    :param transaction: Dataframe pandas; Transaction data ; /input/transactions.csv
    :param account: Dataframe pandas; accounts data ; /input/accounts.csv
    :return: A dataframe pandas; 3 columns : id, nb of transactions and historical day
    '''

    # Calculate number of transaction per person
    data_transac = transaction.groupby("account_id").size()

    # Calculate history of transaction (max - min)
    tab_temp = []
    for i in transaction['account_id'].unique():
        tab_temp.append((account[account['id'] == i]['update_date'].max() - transaction[transaction['account_id'] == i][
            'date'].min()).days)

    data_out = pd.DataFrame(data=data_transac, columns=["nb_transac"])
    data_out.insert(1, "histo_transac", tab_temp)

    return data_out


def keep_180days_history(data_transaction, data_account, indics):
    '''
    Keep id's with more 180 days of history in data_transaction and data_account

    :param data_transaction: Dataframe Pandas: Purge data_transaction, only keep transactions from accounts with more than 183 days of history
    :param data_account: Datframe Pandas: Purge data_account; only keep accounts with more than 183 days of history
    :param indics: Dataframe pandas; It's output from function calculate_indic
    :return: 2 datas: Data_transaction and data_account; same data with only accounts with more than 183 days of history
    '''

    transaction_restreint = data_transaction.loc[
        data_transaction['account_id'].isin(indics.loc[indics['histo_transac'] >= 183, :].index)]
    account_restreint = data_account.loc[data_account['id'].isin(indics.loc[indics['histo_transac'] >= 183, :].index)]

    return account_restreint, transaction_restreint


def database_outgoing(transaction, account, keep=12):
    '''
    Construction of the outgoing base.

    :param transaction: Dataframe pandas of transaction ; ex : output of keep_180days_history
    :param account: Dataframe pandas of account ; ex : output of keep_180days_history
    :param keep: int; This int correspond of times (in month) we want to keep.
    :return: 2 output :
    c6 : Dataframe pandas; This table is used to build the database final (Outgoing monthly)
    c3 : Dataframe pandas; This table is used to build the features
    '''
    # keep only amount negative
    c0 = transaction.loc[transaction['amount'] < 0, :]

    # groupby per id
    c1 = c0.groupby(['date', 'account_id']).sum()

    # rebuild base
    c2 = c1.reset_index()

    # build month - 30days
    c3 = c2.merge(account[["id", "update_date"]], left_on='account_id', right_on='id', how='left').drop(columns=["id"])
    c3["int_30"] = (((c3["update_date"] - c3["date"]).dt.days) / 30).astype(int)

    # keep only 6 months
    c4 = c3[c3["int_30"] <= keep].drop(columns=["date", "update_date"])

    # group by int30
    c5 = c4.groupby(['int_30', 'account_id']).sum()

    # rebuild base
    c6 = c5.reset_index()

    return c6, c3

def build_balance_history(transaction, account, keep=12):
    '''
    Build of balance base which goes back in time
    :param transaction: Dataframe pandas of transaction ; ex : output of keep_180days_history
    :param account: Dataframe pandas of account ; ex : output of keep_180days_history
    :param keep: int; This int correspond of times (in month) we want to keep
    :return: Dataframe pandas; This table is used to build the database final (Balance monthly)
    '''
    x = pd.DataFrame()

    for i in account["id"].unique():
        # Browse each id
        temp_data = transaction[transaction["account_id"] == i]

        # /!\ We choose to achieve the average balance in 1 month
        temp_data2 = temp_data[["date", "amount"]].groupby(['date']).mean()

        # Reverse date
        temp_data3 = temp_data2.sort_values(by='date', ascending=False)

        # Reserve sign
        temp_data3["amount"] = - temp_data3["amount"]

        # Add amount of last date
        temp_data3["amount"][0] = temp_data3["amount"][0] + account[account["id"] == i]['balance']

        # Calculation of cumsum
        temp_reverse = np.cumsum(temp_data3["amount"].values)

        # Add in dataset
        temp_data3.insert(1, "balance", temp_reverse)
        temp_data3.insert(1, "id", i)

        temp_data3 = temp_data3[["id", "balance"]]

        # Reset Index for date
        temp_data3 = temp_data3.reset_index()

        # Concat every id's resultat
        x = x.append(temp_data3)

    x1 = x.merge(account[["id", "update_date"]], on='id', how='left')
    x1["int_30"] = (((x1["update_date"] - x1["date"]).dt.days) / 30).astype(int)
    x2 = x1[x1["int_30"] <= keep].drop(columns=["date", "update_date"])
    x3 = x2.groupby(['int_30', 'id']).mean()
    x4 = x3.reset_index()

    return x4

def arrange_n_months(data, nb_month, type_table):
    '''

    Function used to add a number of month in dataframe.
    Ex: The person only has 8 months of history but my model needs 12 months. So, I build him the months of difference (with value 0)

    :param data: Dataframe pandas; Outgoing data or balance data.
    :param nb_month: Int; Number of month.
    :param type_table: Different variable id for outgoing or balance data so we customize
    :return: The new dataframe with the desired number of months
    '''

    var_id = str()
    var_name_amount = str()

    if type_table == "balance":
        var_id = "id"
        var_name_amount = type_table
    elif type_table == "outgoing":
        var_id = "account_id"
        var_name_amount = "amount"

    for i in data[var_id].unique():
        temp_tab = data[data[var_id] == i]
        diff = list(set(np.arange(1, nb_month + 1, 1)) - set(temp_tab["int_30"]))
        if len(diff) > 0:
            # print(diff)
            for j in diff:
                temp = pd.DataFrame(data=[[j, i, 0.0]], columns=["int_30", var_id, var_name_amount])
                # print(temp)
                data = data.append(temp, ignore_index=True)
                # print(base_balance)

    return data


def create_concat_table(balance, outgoing, training=True):
    '''
    Function which concatenates the base of balance and the base of outgoing
    :param balance: Dataframe pandas; Balance data ; ex : output of build_balance_history
    :param outgoing: Dataframe pandas; Balance data ; ex : output of database_outgoing
    :param training: Variable that allows to differentiate between training and inference.
                     This corresponds to if we build our target
    :return: Dataframe pandas; The concat of 2 tables
    '''

    # We delete in the balance table, int_30 = 0 because we want to predict int_30 outgoing so
    # it's not possible to have int_30 in datatrain
    balance_2 = balance[balance["int_30"] != 0]
    # ingoing_2 = ingoing[ingoing["int_30"] != 0]

    # Balance pivot
    balance_3 = balance_2.pivot(index='id', columns="int_30", values="balance").add_prefix('B')
    balance_4 = balance_3.fillna(0)

    # Outgoing pivot
    outgoing_2 = outgoing.pivot(index='account_id', columns="int_30", values="amount").add_prefix('O')
    outgoing_3 = outgoing_2.fillna(0)
    outgoing_4 = outgoing_3

    # We build the target
    if training:
        outgoing_4 = outgoing_3.rename(columns={"O0": "Y"})

    base_fi = pd.concat([balance_4, outgoing_4], axis=1)

    base_fi_2 = base_fi.fillna(0)

    return base_fi_2


