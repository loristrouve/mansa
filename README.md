# Technical Test - Loris Trouvé

## Description

This is the project of the technical test of Loris Trouvé at Mansa.

The test is organized as follows :
* folder: __input__
    + *accounts.csv*: contains a list of bank accounts, with the date of the last update of their financial data, and the balance on the account on the update date
    + *transactions.csv*: contains all available transactions on the accounts up to the update date, with an amount (in EUR) and the date of the day they were recorded
* folder: __models__
    + *model.joblib.dat*: saved model ready for inference
    + *mean.csv*: csv which is used to center reduce at the same level as the training data
    + *std.csv*: csv which is used to center reduce at the same level as the training data 
* folder: __notebooks__
    + *noteb_mansa.ipynb*: All the exploratory part is present there.
* folder: __src__
    + *config.py*: This file contains the important variables for the performance of the project
    + *create_feature.py*: this file contains the function that creates features for the model
    + *inference.py*: this file contains the function for the inference of the model, in particular at the level of the API
    + *preprocessing.py*: this file contains all the preprocessing functions to have training and inference data
    + *second.py*: This file is the equivalent of a main_bis. This file is the equivalent of a main_bis. 
     It contains all the preprocessing functions, create feature function and training function.
    + *training.py*: This file contains the function for training the model

* *main.py*: the main file that controls all the project. This is your main function with a few additions from me.
* *readme.md*: Describing the project and my approach
* *requirements.txt*: All libraries and versions that I have used
* *test_main.py*: the test file to test the api with 1 example

## Setup

This project run with Python 3.8.12

To setup the environment, you must do:
```bash
pip install -r requirements.txt
```

## Running

To running this app, you must do:

```bash
uvicorn main:app
```

To test the inference, you can run "test_main.py"

To test the training, you can run "second.py"

## Global approach and ideas

### Exploration

At the start of each project, I appreciate do explanatory part to have a better data understanding and the project. This is one of the most important steps for me. You will find a long exploratory part on the notebook.  
You will also see that initially, I had made a base at the level of each month and not by 30 rolling days as you had specified. 
I appreciate not being influenced at the start of a project.  
Also, I asked myself to restrict the data with a new feature: the number of transactions. After having seen your tips on the 180 days of history, I noticed that by restricting to 180 days of history, we also have 180 transactions on average. It seemed sufficient to me so I didnt continue on this way. A plot in my notebook justifies this choice.

### Approach 

At the beginning, when I saw that I was going to work on time data, I immediately thought of time series algorithms and regression algorithms. It is for this reason that you will find in the comments a small approach with Prophet.  
Both approaches should be good.
I have more experience in regression algorithms compare in time series algorithms so I went for regression. 
Also, the fact of working in 30 rolling days guides me to choose a regression. 

### Build database & functions

I have built a lot of preprocessing functions and it corresponds to the preprocessing steps.
I chose to restrict myself to a minimum of 20 months and therefore create a function to have 20 months each time, regardless of whether the person has only 6 months of transactions. I would add 14 months equal to 0. My choice is explained in my notebook where the performance increases with the number of months.

### Modeling

I made the choice to build a regression model that I knew. There are certainly other models that work very well. Knowing that the goal was not perfomance, I spent little time on the subject.

### Opening

I think it would have been a great idea to make a stacking model combining the 2 approaches I described above :
- 1 model with time series algorithms ; (it's the weakness of my model : the temporality of data)
- 1 regression model





